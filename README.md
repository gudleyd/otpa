# One Time Password Authentication (OTPA)

This is 0-dependencies self-contained Swift 5 package for TOPT and HOTP generation from ***otpauth/*** standard uris.

This format is pretty well described here https://github.com/google/google-authenticator/wiki/Key-Uri-Format and it's used by almost every single one web service.

