// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "OTPA",
    platforms: [
        .iOS(.v11),
        .macOS(.v10_12)
    ],
    products: [
        .library(
            name: "OTPA",
            targets: ["OTPA", "Base32"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "OTPA",
            dependencies: ["Base32"]),
        .target(
            name: "Base32",
            dependencies: []),
        .testTarget(
            name: "OTPATests",
            dependencies: ["OTPA"]),
    ]
)
