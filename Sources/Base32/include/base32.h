//
//  base32.h
//  
//
//  Created by Ivan Lebedev on 10.02.2022.
//

#ifndef base32_h
#define base32_h

#include <stdio.h>

int base32_decode(const uint8_t *encoded, uint8_t *result, int bufSize);

#endif /* base32_h */
