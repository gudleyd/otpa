//
//  Crypto.swift
//  
//
//  Created by Ivan Lebedev on 10.02.2022.
//


// Some easy crypto stuff, changed and tested by me, got it from https://github.com/mattrubin/OneTimePassword/blob/develop/Sources/Crypto.swift

import Foundation
import CryptoKit
import CommonCrypto


public enum HMACAlgorithm: String {
    case sha1 = "sha1"
    case sha256 = "sha256"
    case sha512 = "sha512"
    
    @available(iOS 13.0, macOS 10.15, watchOS 6.0, *)
    var hashLength: Int {
        switch self {
        case .sha1:
            return Insecure.SHA1.byteCount
        case .sha256:
            return SHA256.byteCount
        case .sha512:
            return SHA512.byteCount
        }
    }
    
    var hashInfo: (hashFunction: CCHmacAlgorithm, hashLength: Int) {
        switch self {
        case .sha1:
            return (CCHmacAlgorithm(kCCHmacAlgSHA1), Int(CC_SHA1_DIGEST_LENGTH))
        case .sha256:
            return (CCHmacAlgorithm(kCCHmacAlgSHA256), Int(CC_SHA256_DIGEST_LENGTH))
        case .sha512:
            return (CCHmacAlgorithm(kCCHmacAlgSHA512), Int(CC_SHA512_DIGEST_LENGTH))
        }
    }
}

func HMAC(algorithm: HMACAlgorithm, key: Data, data: Data) -> Data {
    if #available(iOS 13.0, macOS 10.15, watchOS 6.0, *) {
        return cryptoKitHMAC(algorithm: algorithm, key: key, data: data)
    } else {
        return commonCryptoHMAC(algorithm: algorithm, key: key, data: data)
    }
}

@available(iOS 13.0, macOS 10.15, watchOS 6.0, *)
private func cryptoKitHMAC(algorithm: HMACAlgorithm, key: Data, data: Data) -> Data {
    let key = SymmetricKey(data: key)

    func createData(_ ptr: UnsafeRawBufferPointer) -> Data {
        Data(bytes: ptr.baseAddress!, count: algorithm.hashLength)
    }

    switch algorithm {
    case .sha1:
        return CryptoKit.HMAC<Insecure.SHA1>.authenticationCode(for: data, using: key).withUnsafeBytes(createData)
    case .sha256:
        return CryptoKit.HMAC<SHA256>.authenticationCode(for: data, using: key).withUnsafeBytes(createData)
    case .sha512:
        return CryptoKit.HMAC<SHA512>.authenticationCode(for: data, using: key).withUnsafeBytes(createData)
    }
}


private func commonCryptoHMAC(algorithm: HMACAlgorithm, key: Data, data: Data) -> Data {
    let (hashFunction, hashLength) = algorithm.hashInfo

    let macOut = UnsafeMutablePointer<UInt8>.allocate(capacity: hashLength)

    defer {
        macOut.deallocate()
    }

    key.withUnsafeBytes { keyBytes in
        data.withUnsafeBytes { dataBytes in
            CCHmac(hashFunction, keyBytes.baseAddress, key.count, dataBytes.baseAddress, data.count, macOut)
        }
    }

    return Data(bytes: macOut, count: hashLength)
}
