//
//  OTPA.swift
//  otp-ios-manager (iOS)
//
//  Created by Ivan Lebedev on 10.02.2022.
//

import Foundation
import Base32

public enum OTPType: Equatable {
    case totp(period: TimeInterval)
    case hotp(counter: UInt64)
    
    public func value(at time: Date) -> UInt64 {
        switch self {
        case .totp(let period):
            let timeSinceEpoch = time.timeIntervalSince1970
            return UInt64(timeSinceEpoch / period)
        case .hotp(let counter):
            return counter
        }
    }
    
    public func validFrom(at time: Date) -> UInt64 {
        switch self {
        case .totp(let period):
            let timeSinceEpoch = time.timeIntervalSince1970
            return UInt64(timeSinceEpoch / period) * UInt64(period)
        case .hotp(_):
            return 0
        }
    }
    
    public func validTo(at time: Date) -> UInt64 {
        switch self {
        case .totp(let period):
            let timeSinceEpoch = time.timeIntervalSince1970
            return (UInt64(timeSinceEpoch / period) + 1) * UInt64(period)
        case .hotp(_):
            return UInt64.max
        }
    }
    
    public mutating func incrementCounter() {
        switch self {
        case .totp(_):
            return
        case .hotp(let counter):
            self = .hotp(counter: counter + 1)
        }
    }
    
    static func from(_ type: String, _ dict: [String:String]) throws -> OTPType {
        if type == OTP.UriKeys.totp {
            if let periodString = dict[OTP.UriKeys.period],
               let period = Int(periodString) {
                return .totp(period: TimeInterval(period))
            }
            return .totp(period: 30)
        }
        if type == OTP.UriKeys.hotp {
            if let counterString = dict[OTP.UriKeys.counter],
               let counter = UInt64(counterString) {
                return .hotp(counter: counter)
            }
            throw OTP.ParsingError.noCounterValueForHOTP
        }
        throw OTP.ParsingError.unknownOtpType
    }
}

public class OTP {
    
    // Display values
    public private(set) var issuer: String?
    public private(set) var account: String?
    
    // Generator values
    public private(set) var type: OTPType
    public private(set) var digits: Int
    public private(set) var algorithm: HMACAlgorithm
    public private(set) var secret: Data
    
    // Original URI
    public private(set) var uri: String

    private var timer: Timer?
    public var onUpdate: ((String) -> ())?
    
    public var currentPasscode: String {
        get {
            self.generatePasscode(at: Date())
        }
    }
    
    private init(issuer: String?, account: String?, type: OTPType, digits: Int, algorithm: HMACAlgorithm, secret: Data, uri: String) throws {
        self.issuer = issuer
        self.account = account
        self.type = type
        self.digits = digits
        self.algorithm = algorithm
        self.secret = secret
        self.uri = uri
        
        self.createTimer()
    }
    
    private func createTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: TimeInterval(type.validTo(at: Date())) - Date().timeIntervalSince1970, repeats: false) { [weak self] _ in
            self?.onUpdate?(self?.currentPasscode ?? "")
            self?.createTimer()
        }
    }
    
    deinit {
        timer?.invalidate()
    }
    
    public func passcode(at time: Date) -> String {
        return generatePasscode(at: time)
    }
    
    public func incrementCounter() {
        type.incrementCounter()
        onUpdate?(currentPasscode)
    }
    
    private func generatePasscode(at time: Date) -> String {
        let counter = type.value(at: time)
        // Ensure the counter value is big-endian
        var bigCounter = counter.bigEndian

        // Generate an HMAC value from the key and counter
        let counterData = Data(bytes: &bigCounter, count: MemoryLayout<UInt64>.size)
        let hash = HMAC(algorithm: algorithm, key: secret, data: counterData)

        var truncatedHash = hash.withUnsafeBytes { ptr -> UInt32 in
            // Use the last 4 bits of the hash as an offset (0 <= offset <= 15)
            let offset = ptr[hash.count - 1] & 0x0f

            // Take 4 bytes from the hash, starting at the given byte offset
            let truncatedHashPtr = ptr.baseAddress! + Int(offset)
            return truncatedHashPtr.bindMemory(to: UInt32.self, capacity: 1).pointee
        }

        // Ensure the four bytes taken from the hash match the current endian format
        truncatedHash = UInt32(bigEndian: truncatedHash)
        // Discard the most significant bit
        truncatedHash &= 0x7fffffff
        // Constrain to the right number of digits
        truncatedHash = truncatedHash % UInt32(pow(10, Float(digits)))

        // Pad the string representation with zeros, if necessary
        return String(truncatedHash).padding(toLength: digits, withPad: "0", startingAt: 0)
    }
}

extension OTP {
    
    public enum UriKeys {
        static let otpauth = "otpauth"
        static let secret = "secret"
        static let issuer = "issuer"
        static let algorithm = "algorithm"
        static let digits = "digits"
        
        static let totp = "totp"
        static let hotp = "hotp"
        static let period = "period"
        static let counter = "counter"
    }
    
    public enum ParsingError: Swift.Error {
        
        // decoding
        case invalidUri
        case notOtpAuth
        case noSecretData
        case noOtpType
        case unknownOtpType
        case noCounterValueForHOTP
        case brokenSecret
        
        // encoding
        case invalidData
        case invalidSymbol
    }
    
    /**
        Builds OTP object by URI. Throws errors if URI is not correct.
    */
    public static func parse(uri: String) throws -> OTP {
        
        guard let url = URL(string: uri) else {
            throw ParsingError.invalidUri
        }
        
        var dict = [String:String]()
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        if let queryItems = components.queryItems {
            for item in queryItems {
                dict[item.name.lowercased()] = item.value!
            }
        }
        
        var issuer: String?
        var account: String?
        var digits: Int = 6
        var algorithm = HMACAlgorithm.sha1
        
        guard url.scheme?.lowercased() == UriKeys.otpauth else {
            throw ParsingError.notOtpAuth
        }
        
        guard let host = url.host?.lowercased() else {
            throw ParsingError.noOtpType
        }
        let otpType = try OTPType.from(host, dict)
        
        let label = url.lastPathComponent.split(separator: ":")
        if label.count == 1 {
            account = String(label.first!).trimmingCharacters(in: .whitespaces)
        } else if label.count == 2 {
            issuer = String(label[0])
            account = String(label[1]).trimmingCharacters(in: .whitespaces)
        }
        
        guard let secret = dict[UriKeys.secret] else {
            throw ParsingError.noSecretData
        }
        let unsafeSecretPointer = UnsafeMutablePointer<Int8>(mutating: (secret as NSString).utf8String)
        let secretCstr = UnsafeRawPointer(unsafeSecretPointer)?.bindMemory(to: UInt8.self, capacity: 2048)
        
        let uint8Pointer = UnsafeMutablePointer<UInt8>.allocate(capacity: 2048)
        uint8Pointer.initialize(to: 0)
        
        let count = Int(base32_decode(secretCstr, uint8Pointer, 2048))
        guard count >= 0 else {
            throw ParsingError.brokenSecret
        }
        let secretData = Data(buffer: UnsafeBufferPointer(start: uint8Pointer, count: count))
        
        if let issuerFromDict = dict[UriKeys.issuer] {
            issuer = issuerFromDict
        }

        if let algorithmFromDict = dict[UriKeys.algorithm]?.lowercased(),
           let hmacAlgorithm = HMACAlgorithm(rawValue: algorithmFromDict) {
            algorithm = hmacAlgorithm
        }
        
        if let digitsFromDict = dict[UriKeys.digits],
           let digitsToInt = Int(digitsFromDict) {
            digits = digitsToInt
        }
        
        return try OTP(issuer: issuer, account: account, type: otpType, digits: digits, algorithm: algorithm, secret: secretData, uri: uri)
    }
    
    /**
        Builds otpauth:// URI. This function doesn't check for validity. Pass returned string to .parse(uri: String) to ensure validity.
    */
    public static func buildUri(issuer: String, account: String, type: OTPType, digits: Int, algorithm: HMACAlgorithm, secret: String) throws -> String {
        var queryItems: [String:String] = [:]
        var components = URLComponents()
        components.scheme = UriKeys.otpauth
        
        guard !issuer.contains(":") && !account.contains(":") else {
            throw ParsingError.invalidSymbol
        }
        components.path = "/\(issuer):\(account)"
        
        queryItems[UriKeys.digits] = String(digits)
        queryItems[UriKeys.algorithm] = algorithm.rawValue.uppercased()
        queryItems[UriKeys.secret] = secret
        queryItems[UriKeys.issuer] = issuer
        
        switch type {
        case .totp(let period):
            components.host = UriKeys.totp
            queryItems[UriKeys.period] = String(UInt64(period))
        case .hotp(let counter):
            components.host = UriKeys.hotp
            queryItems[UriKeys.counter] = String(counter)
        }
        
        components.queryItems = queryItems.map { URLQueryItem(name: $0, value: $1) }
        guard let uri = components.url?.absoluteString else {
            throw ParsingError.invalidData
        }
        return uri
    }
}
