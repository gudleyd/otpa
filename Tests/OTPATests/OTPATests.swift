import XCTest
@testable import OTPA

final class OTPATests: XCTestCase {
    
    func testParsing1() throws {
        let otp = try OTP.parse(uri: "otpauth://totp/ACME%20Co:john@example.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co&algorithm=SHA1&digits=6&period=30")
        XCTAssertEqual(otp.issuer, "ACME Co")
        XCTAssertEqual(otp.account, "john@example.com")
        XCTAssertEqual(otp.algorithm, HMACAlgorithm.sha1)
        XCTAssertEqual(otp.digits, 6)
        XCTAssertEqual(otp.type, .totp(period: 30))
    }
    
    func testParsing2() throws {
        let otp = try OTP.parse(uri: "otpauth://totp/Example:alice@google.com?secret=JBSWY3DPEHPK3PXP&issuer=Example")
        XCTAssertEqual(otp.issuer, "Example")
        XCTAssertEqual(otp.account, "alice@google.com")
        XCTAssertEqual(otp.algorithm, HMACAlgorithm.sha1)
        XCTAssertEqual(otp.digits, 6)
        XCTAssertEqual(otp.type, .totp(period: 30))
    }
    
    func testParsing3() throws {
        let otp = try OTP.parse(uri: "otpauth://totp/Galactic%20Empire:darth.vader@darkside.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co&algorithm=SHA512&digits=8&period=45")
        XCTAssertEqual(otp.issuer, "ACME Co")
        XCTAssertEqual(otp.account, "darth.vader@darkside.com")
        XCTAssertEqual(otp.algorithm, HMACAlgorithm.sha512)
        XCTAssertEqual(otp.digits, 8)
        XCTAssertEqual(otp.type, .totp(period: 45))
    }
    
    func testParsing4() throws {
        let otp = try OTP.parse(uri: "otpauth://totp/darth.vader@darkside.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=Galactic%20Empire&period=60")
        XCTAssertEqual(otp.issuer, "Galactic Empire")
        XCTAssertEqual(otp.account, "darth.vader@darkside.com")
        XCTAssertEqual(otp.algorithm, HMACAlgorithm.sha1)
        XCTAssertEqual(otp.digits, 6)
        XCTAssertEqual(otp.type, .totp(period: 60))
    }
    
    func testParsing5() throws {
        let otp = try OTP.parse(uri: "otpauth://hotp?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&counter=0")
        XCTAssertEqual(otp.issuer, nil)
        XCTAssertEqual(otp.account, nil)
        XCTAssertEqual(otp.algorithm, HMACAlgorithm.sha1)
        XCTAssertEqual(otp.digits, 6)
        XCTAssertEqual(otp.type, .hotp(counter: 0))
    }
    
    func testBuildUriAndGenerate() throws {
        let uri = try OTP.buildUri(issuer: "My Own Company", account: "me@example.com", type: .totp(period: 30), digits: 6, algorithm: .sha1, secret: "HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ")
        let otp = try OTP.parse(uri: uri)
        XCTAssertEqual(otp.issuer, "My Own Company")
        XCTAssertEqual(otp.account, "me@example.com")
        XCTAssertEqual(otp.algorithm, HMACAlgorithm.sha1)
        XCTAssertEqual(otp.digits, 6)
        XCTAssertEqual(otp.type, .totp(period: 30))
        
        XCTAssertEqual(otp.passcode(at: Date(timeIntervalSince1970: 1644495361)), "735913")
    }
    
    func testGeneration1() throws {
        let otp = try OTP.parse(uri: "otpauth://totp/ACME%20Co:john@example.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co&algorithm=SHA1&digits=6&period=30")
        XCTAssertEqual(otp.passcode(at: Date(timeIntervalSince1970: 1644495361)), "735913")
    }
    
    func testGeneration2() throws {
        let otp = try OTP.parse(uri: "otpauth://totp/ACME%20Co:john@example.com?secret=UGHTOWJTIRJTLRSDLGJREI&issuer=ACME%20Co&algorithm=SHA1&digits=6&period=30")
        XCTAssertEqual(otp.passcode(at: Date(timeIntervalSince1970: 1644673505)), "256996")
    }
    
    func testGeneration3() throws {
        let otp = try OTP.parse(uri: "otpauth://totp/ACME%20Co:john@example.com?secret=AAAABBBBCCCCDDDD&issuer=ACME%20Co&algorithm=SHA1&digits=6&period=30")
        XCTAssertEqual(otp.passcode(at: Date(timeIntervalSince1970: 1644673774)), "621046")
    }
    
    func testPerformanceCreationFromUri() {
        let uri = "otpauth://totp/ACME%20Co:john@example.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co&algorithm=SHA1&digits=6&period=30"
        
        measure {
            for _ in 0..<50_000 {
                let _ = try! OTP.parse(uri: uri)
            }
        }
    }
    
    func testPerformancePasscodeGeneration() {
        let otp = try! OTP.parse(uri: "otpauth://totp/ACME%20Co:john@example.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co&algorithm=SHA1&digits=6&period=30")
        
        measure {
            for _ in 0..<350_000 {
                let _ = otp.currentPasscode
            }
        }
    }
}
